package com.saprem.springboot.restapi2.survey;

import java.net.URI;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
public class SurveyResource {

	private SurveyService surveyService;

	public SurveyResource(SurveyService surveyService) {
		super();
		this.surveyService = surveyService;
	}

	@RequestMapping("/surveys")
	public List<Survey> retrieveAllSurveys() {
		return surveyService.retrieveAllSurveys();
	}
	@RequestMapping("/surveys/{surveyId}")
	public Survey retrieveSurveyById(@PathVariable String surveyId) {
		return surveyService.retrieveSurveyById(surveyId);
	}
	@RequestMapping("/surveys/{surveyId}/questions")
	public List<Question> retrieveAllQuestions(@PathVariable String surveyId) {
		List<Question> questions = surveyService.retrieveAllSurveyQuestions(surveyId);
		
		return questions;
	}
	@RequestMapping("/surveys/{surveyId}/questions/{questionId}")
	public Question retrieveSpecificSurveyQuestion(@PathVariable String surveyId,@PathVariable String questionId) {
		Question question = surveyService.retrieveSpecificSurveyQuestion(surveyId,questionId);
		
		return question;
	}
	@RequestMapping(value="/surveys/{surveyId}/questions",  method=RequestMethod.POST)
	public ResponseEntity<Object> addnewSurveyQuestion(@PathVariable String surveyId,@RequestBody Question question) {
		surveyService.addNewSurveyQuestion(surveyId,question);
		
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{questionId}").buildAndExpand(question).toUri();
		return ResponseEntity.created(location ).build();
	}
	@RequestMapping("/surveys/{surveyId}/questions/{questionId}")
	public Question deleteSurveyQuestion(@PathVariable String surveyId,@PathVariable String questionId) {
		surveyService.deleteSurveyQuestion(surveyId,questionId);
		
		return ResponseEntity.noContent().build();
	}
	
}
