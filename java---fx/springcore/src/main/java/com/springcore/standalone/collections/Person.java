package com.springcore.standalone.collections;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
@Component
@Scope("prototype")
public class Person {
private List<String>friends;

@Override
public String toString() {
	return "Person [friends=" + friends + "]";
}

public List<String> getFriends() {
	return friends;
}

public void setFriends(List<String> friends) {
	this.friends = friends;
}
}
