package com.springcore.standalone.collections;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.ApplicationContext;
public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ApplicationContext am = new ClassPathXmlApplicationContext("com/springcore/standalone/collections/stconfig.xml");
		Person person1 = (Person)am.getBean("person",Person.class);
		System.out.println(person1);
		System.out.println(person1.hashCode());
		Person person2 = am.getBean("person",Person.class);
		System.out.println(person2.hashCode());
	}

}
