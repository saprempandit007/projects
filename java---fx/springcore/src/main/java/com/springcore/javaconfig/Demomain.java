package com.springcore.javaconfig;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Demomain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ApplicationContext con = new AnnotationConfigApplicationContext(JavaConfig.class);
		Student st = con.getBean("getStudent", Student.class);
		System.out.println(st);
		st.stud();
	}

}
