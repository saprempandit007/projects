package com.springcore.lifecycle;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import org.springframework.context.support.ClassPathXmlApplicationContext;
public class Test {

	public static void main(String[] args) {
		AbstractApplicationContext context = new ClassPathXmlApplicationContext("com/springcore/lifecycle/sconfig.xml");
		samosa s1 = (samosa)context.getBean("chutney");
		context.registerShutdownHook();
		System.out.println(s1);
		Pepsi pep = (Pepsi) context.getBean("pepsi");
		System.out.println(pep);
		
		
	}

}
