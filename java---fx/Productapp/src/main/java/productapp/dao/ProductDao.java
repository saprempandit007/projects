package productapp.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import productapp.model.Product;

@Component
public class ProductDao {
	@Autowired
private HibernateTemplate hibernateTemplate;
	@Transactional
public void createProduct(Product product) {
	this.hibernateTemplate.save(product);
}
	public List<Product> getAllProducts(){
		List<Product> Product = this.hibernateTemplate.loadAll(Product.class);
		return Product;
	}
	@Transactional
	public void deleteProduct(int pid) {
		Product p = this.hibernateTemplate.load(Product.class, pid);
		this.hibernateTemplate.delete(p);
	}
	public Product getProduct(int pid) {
		return this.hibernateTemplate.get(Product.class, pid);
		
		
	}
}
