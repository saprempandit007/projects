<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<%@include file="./base.jsp"%>
<title>Product CRUD APP</title>
</head>
<body>
	<div class="container mt-3">
		<div class="row">
			<div class="col-md-6 offset-md-3">
				<h1 class="text-center mb-3">Fill the product details</h1>
				<form action="handle-product" method="post">
					<div class="form-group">
						<label for="name">Product Name</label> <input type="text"
							class="form-control" id="name" name="name"
							aria-describedby="emailHelp"
							placeholder="Enter your product name">
					</div>
					<div class="form-group">
						<label for="Description">Description</label>
						<textarea class="form-control" name="Description" id="Description"
							rows="" cols="" placeholder="Enter the product description">
						</textarea>

					</div>
					<div class="form-group">
						<label for="price">Price of the product</label> <input type="text"
							class="form-control" id="price" name="price"
							aria-describedby="emailHelp"
							placeholder="Enter your product's price">
					</div>
					<div class="container text-center">
					<a href="${pageContext.request.contextPath}/" class="btn btn-outline-danger">Back</a>
					  <button type="submit" class="btn btn-primary">Add</button>
					</div>
					</form>
			</div>
		</div>
	</div>
</body>
</html>