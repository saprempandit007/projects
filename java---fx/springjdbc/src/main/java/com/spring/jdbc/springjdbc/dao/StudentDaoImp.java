package com.spring.jdbc.springjdbc.dao;

import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.spring.jdbc.springjdbc.entities.Student;

public class StudentDaoImp implements StudentDao{

	private JdbcTemplate jdbctemplate;

	public JdbcTemplate getJdbctemplate() {
		return jdbctemplate;
	}

	public void setJdbctemplate(JdbcTemplate jdbctemplate) {
		this.jdbctemplate = jdbctemplate;
	}

	@Override
	public int insert(Student student) {
		String q = "insert into student(id,name,city) values(?,?,?)";
		int r = this.jdbctemplate.update(q,student.getId(),student.getName(),student.getCity());
		return r;
	}

	@Override
	public int change(Student student) {
		String q = "update student set name=?,city=? where id=?";
		int r = this.jdbctemplate.update(q,student.getName(),student.getCity(),student.getId());
		return r;
	}

	@Override
	public Student getStudent(int studentId) {
		String q="select * from student where id=?";
		RowMapper<Student> rowMapper = new RowMapperImpl();
		Student student = this.jdbctemplate.queryForObject(q,rowMapper,studentId);
		return student;
	}

	@Override
	public List<Student> getAllStudents() {
		String q = "select * from student";
		List<Student> students = this.jdbctemplate.query(q, new RowMapperImpl());
		return students;
	}
	

}
	


