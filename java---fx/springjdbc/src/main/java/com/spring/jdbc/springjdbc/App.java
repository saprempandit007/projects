package com.spring.jdbc.springjdbc;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

import com.spring.jdbc.springjdbc.dao.StudentDao;
import com.spring.jdbc.springjdbc.entities.Student;

public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "My Program started !" );
        ApplicationContext con = new ClassPathXmlApplicationContext("com/spring/jdbc/springjdbc/jdbcconfig.xml");
        StudentDao studentDao = con.getBean("StudentDao",StudentDao.class);
//        Student student = new Student();
//        student.setId(777);
//        student.setName("Sapp");
//        student.setCity("satara");
//        int result = studentDao.change(student);
//        System.out.println("Student"+result);
//    	Student student = studentDao.getStudent(7);
        List<Student> student = studentDao.getAllStudents();

for(Student s :student) {
	System.out.println(s);
}
    }
}
