package com.spring.orm;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.spring.orm.dao.StudentDao;
import com.spring.orm.entities.Student;

import java.util.List;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {
		ApplicationContext con = new ClassPathXmlApplicationContext("config.xml");
		StudentDao studentDao = con.getBean("studentDao", StudentDao.class);
//        Student student = new Student(001,"Sandhya Pandit","pune");
//        int r = studentDao.insert(student);
//        System.out.println("done"+r);
		Scanner sc = new Scanner(System.in);
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		boolean go = true;
		while (go) {
			System.out.println("PRESS 1 : To add new Student");
			System.out.println("PRESS 2 : To Show details of all students");
			System.out.println("PRESS 3 : To get detail of a single student");
			System.out.println("PRESS 4 : To delete a record of a Student");
			System.out.println("PRESS 5 : To update a record");
			System.out.println("PRESS 6 : To exit :) ");
			try {
				int input = Integer.parseInt(br.readLine());
				switch (input) {
				case 1:
					System.out.println("Enter student id :");
					int id = sc.nextInt();
					System.out.println("Enter student name :");
					String name = sc.next();
					System.out.println("Enter the city :");
					String city = sc.next();
					Student student = new Student();
					student.setStudentId(id);
					student.setStudentName(name);
					student.setStudentCity(city);
					int r = studentDao.insert(student);
					System.out.println(r+"Student added");
					break;
				case 2:
					List<Student> allStudents = studentDao.getAllStudent();
					for(Student st:allStudents ) {
						System.out.println("ID :"+st.getStudentId());
						System.out.println("Name :"+st.getStudentName());
						System.out.println("City :"+st.getStudentCity());
						System.out.println("_________________________________________");
					}
					break;
				case 3:
					int id1 = sc.nextInt();
					Student st1 = studentDao.getStudent(id1);
					System.out.println("ID :"+st1.getStudentId());
					System.out.println("Name :"+st1.getStudentName());
					System.out.println("City :"+st1.getStudentCity());
					break;
				case 4:
					int id2 = sc.nextInt();
					studentDao.delete(id2);
					System.out.println("Deleted");
					break;
				case 5:
					int id3 = sc.nextInt();
					studentDao.update(id3);
					break;
				case 6:
					go=false;
					
					break;
				}
			} catch (Exception e) {
				System.out.println("Invalid input please try again");
				System.out.println(e.getMessage());
			}
		}
		System.out.println("Thank you !!!");
	}
}
