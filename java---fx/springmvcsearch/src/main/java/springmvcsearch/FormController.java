package springmvcsearch;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class FormController {

	@RequestMapping("/complex")
	public String showForm() {
		
		return "compform";
	}
	@RequestMapping("/handleform")
	public String formHandler(@ModelAttribute FormDetails formDetails,BindingResult binding) {
		if(binding.hasErrors()) {
			return "compform";
		}
		System.out.println(formDetails);
		return "success";
		
	}
}
