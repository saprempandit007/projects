<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>This is JSTL CoreTags</h1>
        <!--Set tag-->
        <c:set var="i" value="23" scope="application"></c:set>
        <!--Out Tag-->
        <h1><c:out value="${i}"></c:out></h1>
        <!--Remove Tag-->
        <%--<c:remove var="i"></c:remove>--%>
        <h1><c:out value="${i}">This is default value</c:out></h1>
        <hr><!-- comment -->
        <!--if Tag-->
        <c:if test="${i==23}">
            <h1>condition is true i == 23</h1>
            <p>Hello welcome to CoreTags......</p>
            <hr>
        </c:if>
        <c:choose>
            <c:when test="${i<0}">
                <h1>i is negative </h1>
            </c:when>
            <c:when test="${i>0}">
                <h1>i is positive</h1>
            </c:when>
            <c:otherwise>
                <h1>Default case</h1>
            </c:otherwise>
        </c:choose> 
    </body> 
</html>
