<%@page import="com.insane.blog.helper.ConnectionProvider"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
        <link href="css/Mystyles.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <style>
            .banner-background{
                clip-path: polygon(30% 0%, 70% 0%, 100% 0, 100% 87%, 65% 100%, 26% 89%, 0 100%, 0 0);
            }
        </style>
    </head>
    <body> 
        <%@include file="normalnavbar.jsp" %>
        <div class="container-fluid p-0 m-0">
            <div class="jumbotron bg-secondary text-dark banner-background">
                <div class="container">
                    <h3>Welcome to InsaneBlog</h3>
                    <p>Welcome to InsaneBlog,world of programming knowledge
                        Coding cultivates analytical skills, enhances critical thinking, and fosters adaptability—skills that are highly transferable to various job roles. Moreover, it enables professionals to automate tasks, improving efficiency in their work  
                    </p>
                    <br><!-- comment -->
                    <br><!-- comment -->
                    <button type="button" class="btn btn-dark"><span class="fa fa-external-link-square"></span>  Start ! Its free</button>
                    <a href="loginpage.jsp" class="btn btn-dark"><span class="fa fa-user-circle-o fa-spin"></span>  Login </a>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row mb-2">
                <div class="col-md-4">
                    <div class="card bg-secondary">
                        <div class="card-body">
                            <h5 class="card-title">java Programming</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-dark">Go somewhere</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card bg-secondary">
                        <div class="card-body">
                            <h5 class="card-title">java Programming</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-dark">Go somewhere</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card bg-secondary">
                        <div class="card-body">
                            <h5 class="card-title">java Programming</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-dark">Go somewhere</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="card bg-secondary">
                        <div class="card-body">
                            <h5 class="card-title">java Programming</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-dark">Go somewhere</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card bg-secondary">
                        <div class="card-body">
                            <h5 class="card-title">java Programming</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-dark">Go somewhere</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card bg-secondary">
                        <div class="card-body">
                            <h5 class="card-title">java Programming</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-dark">Go somewhere</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.7.1.min.js" integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.min.js" integrity="sha384-+sLIOodYLS7CIrQpBjl+C7nPvqq+FbNUBDunl/OZv93DB7Ln/533i8e/mZXLi/P+" crossorigin="anonymous"></script>
        <script src="js/MyJs.js" type="text/javascript"></script>
    </body>
</html>
