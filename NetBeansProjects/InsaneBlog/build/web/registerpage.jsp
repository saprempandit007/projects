<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Sign up </title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
        <!--<link href="css/Mystyles.css" rel="stylesheet" type="text/css"/>-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <style>
            .banner-background{
                clip-path: polygon(30% 0%, 70% 0%, 100% 0, 100% 87%, 65% 100%, 26% 89%, 0 100%, 0 0);
            }
        </style>
    </head>
    <body>
        <%@include file="normalnavbar.jsp" %>
        <main class="bg-secondary banner-background " style="padding-bottom: 80px;">
            <div class="container">
                <div class="col-md-4 offset-md-4">
                    <div class="card mt-0">
                        <div class="card-header text-center bg-dark text-light">
                            <span class="fa fa-user-circle fa-2x"></span>
                            <br><!-- comment -->
                            <p>Register Here</p>
                        </div>
                        <div class="card-body">
                            <form id="reg-form" action="RegisterServlet" method="POST">
                                <div class="form-group">
                                    <label for="user_name">User Name</label>
                                    <input name="user_name" type="text" class="form-control" id="user_name" aria-describedby="emailHelp" placeholder="Enter Name">            
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Email address</label>
                                    <input name="user_email" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputPassword1">Password</label>
                                    <input name="user_password" type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                                </div>
                                <div class="form-group">
                                    <label for="gender">Gender</label>
                                    <br><!-- comment -->

                                    <input type="radio" id="gender" name="gender" value="male">Male
                                    <input type="radio" id="gender" name="gender" value="female">female
                                </div>
                                <div class="form-check">
                                    <input name="check" type="checkbox" class="form-check-input" id="exampleCheck1">
                                    <label class="form-check-label" for="exampleCheck1">Agree terms and conditions</label>

                                </div>
                                <br>
                                <div class="container text-center" id="loader" style="display: none;"><!-- comment -->
                                    <span class="fa fa-refresh fa-spin fa-4x"></span>
                                    <h4>Please Wait....</h4>
                                </div>


                                <button type="submit" id="submit-btn" class="btn btn-dark btn-outline-light">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </main>






        <script src="https://code.jquery.com/jquery-3.7.1.min.js" integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.min.js" integrity="sha384-+sLIOodYLS7CIrQpBjl+C7nPvqq+FbNUBDunl/OZv93DB7Ln/533i8e/mZXLi/P+" crossorigin="anonymous"></script>
        <!--<script src="js/MyJs.js" type="text/javascript"></script>-->
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script>
            
                    $(document).ready(function (){
                    console.log("loaded");
                    $('#reg-form').on('submit', function(event){
                    event.preventDefault();
                    let form = new FormData(this);
                    $("#submit-btn").hide();
                    $("#loader").show();
                    $.ajax({
                    url:"RegisterServlet",
                    type:'POST',
                    data:form,
                    success:function(data, textStatus, jqXHR){
                    console.log(data)
                    if (data.trim() === 'Done'){
                    $("#submit-btn").show();
                    $("#loader").hide();
                    swal("registered successfully.. we are redirecting to login page")
                            .then((value) => {
                            window.location = "loginpage.jsp "

                            });
                    } else{
                        
                        swal(data);
                    }
                    },
                    error:function(jqXHR, textStatus, errorThrown){
                    console.log(jqXHR)
                    $("#submit-btn").show();
                    $("#loader").hide();
                    swal("registered successfully.. we are redirecting to login page")
                    },
                    processData:false,
                    contentType:false
            });
            });
            });
        </script>

    </body>
</html>
