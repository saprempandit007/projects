<%@page import="java.util.ArrayList"%>
<%@page import="com.insane.blog.entities.Categories"%>
<%@page import="com.insane.blog.helper.ConnectionProvider"%>
<%@page import="com.insane.blog.entities.Message"%>
<%@page import="com.insane.blog.entities.User"%>
<%@page import="com.insane.blog.dao.PostDao"%>
<%@page errorPage="errorpage.jsp" %>
<%
    User user = (User) session.getAttribute("currentuser");
    if (user == null) {
        response.sendRedirect("loginpage.jsp");
    }
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>*Profile*</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
        <link href="css/Mystyles.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <style>
            .banner-background{
                clip-path: polygon(30% 0%, 70% 0%, 100% 0, 100% 87%, 65% 100%, 26% 89%, 0 100%, 0 0);
            }
/*            nav{
                background-color: burlywood;
            }*/
        </style>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand" href="index.jsp"><span class="fa fa-ge"></span>InsaneBlog</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#"><span class="fa 	fa fa-bell"></span>Learn Code With Saprem<span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item dropdown ">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="fa fa-chevron-circle-down"></span> Categories
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#">Data Structures</a>
                            <a class="dropdown-item" href="#">Programming Languages</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Project Implementation </a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#"><span class="fa fa-address-card-o"></span>Contact</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-toggle="modal" data-target="#add-post-modal"><span class="fa fa-asterisk"></span>Do Post</a>
                    </li>



                </ul>
                <ul class="navbar-nav mr-right">
                    <li class="nav-item">
                        <a class="nav-link" href="#!" data-toggle="modal" data-target="#profile-modal"><span class="fa fa-user-circle"></span><%=user.getName()%> </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="LogoutServlet"><span class="fa fa-user-plus"></span> Log Out </a>
                    </li>
                </ul>
            </div>
        </nav> 
        <%
            Message m = (Message) session.getAttribute("msg");
            if (m != null) {
        %>
        <div class="alert alert-danger" role="alert">
            <%= m.getContent()%>
        </div>
        <%
                session.removeAttribute("msg");
            }
        %>
        <main style="background-image: url(img/bg3.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 mt-4">
                        <div class="list-group">
                            <a href="#" style="background-color: #dbd7d2;" onclick="getPosts(0.this)" class= " c-link list-group-item list-group-item-action active" aria-current="true"><b>
                                All Posts
                                </b>
                            </a>
                            <%
                                PostDao d = new PostDao(ConnectionProvider.getConnection());
                                ArrayList<Categories> list1 = d.getAllCategories();
                                for (Categories cc : list1) {


                            %>
                            <a href="#" style="background-color: #dbd7d2;" onclick="getPosts(<%= cc.getCid()%>,this)" class="c-link list-group-item list-group-item-action"><b><%= cc.getName()%></b></a>
                            <%
                                }

                            %>


                        </div>
                    </div>

                    <div class="col-md-8" >
                        <div class="container text-center" id="loader">
                            <i class="fa fa-refresh fa-3x fa-spin"></i>
                            <h3 class="mt-2">Loading ....</h3>
                        </div>
                        <div class="container-fluid" id="post-container">

                        </div>
                    </div>
                </div>
            </div>
        </main>


        <!-- Modal -->
        <div class="modal fade bg-secondary" id="profile-modal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-dark text-light">
                        <h5 class="modal-title " id="exampleModalLabel">Insane</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="container text-center">
                            <img src="pics/<%= user.getProfile()%>" class="img-fluid" style="border-radius:50%;max-width:150px; ">
                            <br>
                            <h5 class="modal-title" id="exampleModalLabel"><%= user.getName()%></h5>
                            <div id="profile-details">
                                <table class="table table-striped">
                                    <tbody>
                                        <tr>
                                            <th scope="row">ID :</th>
                                            <td><%= user.getId()%></td>

                                        </tr>
                                        <tr>
                                            <th scope="row">Email :</th>
                                            <td><%= user.getEmail()%></td>

                                        </tr>
                                        <tr>
                                            <th scope="row">Gender :</th>
                                            <td><%= user.getGender()%></td>

                                        </tr>
                                        <tr>
                                            <th scope="row">Registered on :</th>
                                            <td><%= user.getDateTime().toString()%></td>

                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div id="profile-edit" style="display:none;">
                                <h2 class="mt-2">Please Edit Carefully</h2>
                                <form action="EditServlet" method="POST" enctype="multipart/form-data">
                                    <table class="table">
                                        <tr>
                                            <td>Change Profile :</td>
                                            <td><input type="file" name="image" class="form-control"></a></td>
                                        </tr>
                                        <tr>
                                            <td>ID :</td>
                                            <td><%= user.getId()%></td>
                                        </tr>
                                        <tr>
                                            <td>Email :</td>
                                            <td><input type="email" class="form-control" name="user_email" value="<%= user.getEmail()%>"</td>
                                        </tr>
                                        <tr>
                                            <td>Name :</td>
                                            <td><input type="text" class="form-control" name="user_name" value="<%= user.getName()%>"</td>
                                        </tr>
                                        <tr>
                                            <td>Password :</td>
                                            <td><input type="password" class="form-control" name="user_password" value="<%= user.getPassword()%>"</td>
                                        </tr>
                                        <tr>
                                            <td>Gender :</td>
                                            <td><%= user.getGender()%></td>
                                        </tr>

                                    </table>
                                    <div class="container">
                                        <button class="btn btn-secondary btn-outline-dark text-light">Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button id="edit-profile-button" type="button" class="btn btn-secondary">Edit</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="add-post-modal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Provide Post Details... </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form id="add-post-form" action="AddPostServlet" method="POST">
                            <div class="form-group">
                                <select class="form-control" name="cid">
                                    <option selected disabled>*** Select Category ***</option>
                                    <%
                                        PostDao postd = new PostDao(ConnectionProvider.getConnection());
                                        ArrayList<Categories> list = postd.getAllCategories();
                                        for (Categories c : list) {


                                    %>
                                    <option value="<%=c.getCid()%>"><%= c.getName()%></option>
                                    <%
                                        }
                                    %>
                                </select>
                            </div>

                            <div class="form-group">
                                <input name="pTitle" type="text" placeholder="Enter the Post Title" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <textarea name="pContent" placeholder="Enter the post content" style="height: 100px"class="form-control"></textarea>
                            </div>
                            <div class="form-group">
                                <textarea name="pCode" placeholder="Enter the post program (if any)" style="height: 100px"class="form-control"></textarea>
                            </div>
                            <div class="form-group">
                                <label>Select your pic : </label>
                                <br>
                                <input name="pic" type="file">
                            </div>
                            <div class="container text-center">
                                <button type="submit" class="btn-dark text-light btn-outline-danger">Post</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>







        <script src="https://code.jquery.com/jquery-3.7.1.min.js" integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.min.js" integrity="sha384-+sLIOodYLS7CIrQpBjl+C7nPvqq+FbNUBDunl/OZv93DB7Ln/533i8e/mZXLi/P+" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
        <script src="js/MyJs.js" type="text/javascript"></script>
        <script>
            $(document).ready(function () {
                let editstatus = false;
                $('#edit-profile-button').click(function () {
                    if (editstatus == false) {
                        $("#profile-details").hide()
                        $("#profile-edit").show();
                        editstatus = true;
                        $(this).text("Back")
                    } else {
                        $("#profile-details").show()
                        $("#profile-edit").hide();
                        editstatus = false;
                        $(this).text("Edit")

                    }

                })


            });





        </script>
        <script>
            $(document).ready(function (e) {
                $("#add-post-form").on("submit", function (event) {
                    event.preventDefault();
                    console.log("you clicked on submit button");

                    let form = new FormData(this);

                    $.ajax({
                        url: "AddPostServlet",
                        type: 'POST',
                        data: form,
                        success: function (data, textStatus, jqXHR) {
                            console.log(data);
                            if (data.trim() == 'done') {
                                swal("Good job!", "Saved successfully", "success");
                            } else {
                                swal("Error", "Something went Wrong!!! try again", "error");

                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            swal("Error", "Something went Wrong!!! try again", "error");

                        },
                        processData: false,
                        contentType: false
                    });
                });
            });
        </script>
        <script>
            function getPosts(catId,temp){
                $("#loader").show();
                $("#post-container").hide();
                $(".c-link").removeClass('active')
                 $.ajax({
                    url: "load_posts.jsp",
                    data:{cid:catId},
                    success: function (data, textStatus, jqXHR) {
                        console.log(data);
                        $("#loader").hide();
                        $("#post-container").show();
                        $("#post-container").html(data);
                        $(temp).addClass('active')
                    },
                    error: function (jqXHR, textStatus, errorThrown) {

                    }

                });
            }
            $(document).ready(function (e) {
                let allPostRef = $('.c-link')[0]
               getPosts(0,allPostRef);
            });
        </script>
    </body>
</html>
