<%@page import="com.insane.blog.dao.LikeDao"%>
<%@page import="java.util.List"%>
<%@page import="com.insane.blog.entities.Post"%>
<%@page import="com.insane.blog.dao.PostDao"%>
<%@page import="com.insane.blog.helper.ConnectionProvider"%>
<div class="row mt-2" style="margin-top: 10px;">
<%
    PostDao d = new PostDao(ConnectionProvider.getConnection());
    int cid = Integer.parseInt(request.getParameter("cid"));
     List<Post> posts = null;
    if(cid == 0){
    posts = d.getAllPosts();
    }else{
    posts = d.getPostByCatId(cid);
    }
    if(posts.size()==0){
    out.println("<h3 class='display-3 text-center'>No posts in this category</h3>");
    return;
    }
    for(Post p:posts){
    %>
    <div class="col-md-6 mt-2">
        <div class="card">
            <img src="blog_pics/<%=p.getpPic()%>" class="card-img-top" alt="...">
            <div class="card-body" style="background-color: #dbd7d2;">
                <b><%= p.getpTitle()%></b>
                <p><%= p.getpContent()%></p>
               
            </div>
                <div class="card-footer" style="background-color: #dbd7d2;">
                    
                    <a href="show_blog_page.jsp?post_id=<%= p.getPid()%>" class="btn btn-outline-dark btn-sm">Read More...</a>
                    <%
                                LikeDao ld = new LikeDao(ConnectionProvider.getConnection());
                                
                            %>
                            <a href="#!" class="btn btn-outline-dark btn-sm" onclick="doLike(<%=p.getPid()%>,<%=p.getUserId()%>)"><i class="fa fa-thumbs-o-up"></i><span class="like-counter"><%=ld.countLikeOnPost(p.getPid())%></span></a>
                    <a href="#!" class="btn btn-outline-dark btn-sm"><i class="fa fa-commenting-o"></i><span>10</span></a>
                </div>
    </div>
    </div>
    
    
    <%
    }
    
    %>
</div>