<%-- 
    Document   : newjsp
    Created on : 07-Jan-2024, 10:25:32 am
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="p" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP PAGE</title>
    </head>
    <body>
        <<h1>Welcome to my error page : </h1>
        <hr>
        <%!
            int a = 10;
            int b = 20;
        %>
        <%
            int division = a/b;
        %>
        <%= division %>
    </body>
</html>
