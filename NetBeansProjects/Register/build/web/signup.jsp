<%-- 
    Document   : signup
    Created on : 27-Dec-2023, 5:04:00 pm
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>SignUp</title>
         <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    </head>
    <body style="background-image:url('img/Screenshot 2023-12-27 173920.jpg'); background-size: cover; background-attachment: fixed;">
        <div class="container">
            <div class="row">
                <div class="col m6 offset-m3">
                    <div class="card">
                    <div class="card-content">
                        <h5>Register here</h5>
                        <div class="form center-align">
                            <form action="Register" method="post">
                                <input type="text" name="user_name" placeholder="Enter the Username"/>   
                                <input type="password" name="user_password" placeholder="Enter the Password"/>
                                <input type="email" name="user_email" placeholder="Enter the Email"/>
                                <button type="submit" class="btn black">Submit</button>
                            </form>
                        </div>
                        <div class="loader center-align" style="margin-top: 20px; display: none">
                              <div class="preloader-wrapper big active">
                                  <div class="spinner-layer spinner-blue-only">
                                      <div class="circle-clipper left">
                                          <div class="circle"></div>
                                      </div><div class="gap-patch">
                                          <div class="circle"></div>
                                      </div><div class="circle-clipper right">
                                          <div class="circle"></div>
                                      </div>
                                  </div>
                              </div>

                              <div class="preloader-wrapper active">
                                  <div class="spinner-layer spinner-red-only">
                                      <div class="circle-clipper left">
                                          <div class="circle"></div>
                                      </div><div class="gap-patch">
                                          <div class="circle"></div>
                                      </div><div class="circle-clipper right">
                                          <div class="circle"></div>
                                      </div>
                                  </div>
                              </div>

                              <div class="preloader-wrapper small active">
                                  <div class="spinner-layer spinner-green-only">
                                      <div class="circle-clipper left">
                                          <div class="circle"></div>
                                      </div><div class="gap-patch">
                                          <div class="circle"></div>
                                      </div><div class="circle-clipper right">
                                          <div class="circle"></div>
                                      </div>
                                  </div>
                              </div>
        

                            
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
            <script src="https://code.jquery.com/jquery-3.7.1.min.js" integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
            <script>
                $(document).ready(function(){
                    console.log("page is ready");
                    $("myform").on('submit',function(event)){
                        event.preventDefault();
                        var f = $(this).serialize();
                        console.log(f);
                        $(".loader").show();
                        $(".form").hide();
                        $.ajax({
                            url:"Register",
                            data:f,
                            type:'POST',
                            success:function(data,textStatus,jqXHR){
                                console.log(data);
                                console.log("Success................");
                                $(".loader").hide();
                                $(".form").show();
                                
                            },
                            error:function(jqXHR,textStatus,errorThrown){
                                console.log(data);
                                console.log("errorr.........");
                                $(".loader").hide();
                                $(".form").show();
                            }
                        });
                    }
                });
            </script>
    </body>
</html>
