/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Servlets;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.*;
public class FirstServlet implements Servlet{
    //life cycle methods
    ServletConfig conf;
    public void init(ServletConfig conf){
        this.conf=conf;
        System.out.println("Creating Object");
    }
    public void service(ServletRequest req,ServletResponse resp)throws ServletException,IOException{
        System.out.println("Servicing");
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        out.println("<h1>this is my output</h1>");
        out.println("<h1>Todays date is "+new Date().toString()+"</h1>");
    }
    public void destroy(){
        System.out.println("Going to destroy servlet");
    }
    public ServletConfig getServletConfig(){
        return this.conf;
        
    }
    public String getServletInfo(){
        return "This servlet is created by saprem";
    }
}
