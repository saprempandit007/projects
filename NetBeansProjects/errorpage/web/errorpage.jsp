<%-- 
    Document   : errorpage
    Created on : 07-Jan-2024, 4:57:19 pm
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page isErrorPage="true" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Sorry Something went wrong...</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        
    </head>
    <body>
        <div class="container p-3 text-center ">
            <img src="img/erorr.jpg" class="img-fluid"/>
            <p><%= exception %>
                <a class="btn btn-outline-warning " href="index.html">Home page</a>
        </div>
        <h1 class="display-3 text-center">Sorry something went wrong</h1>
    </body>
</html>
