/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tags;

import java.util.Date;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

public class MyTagHandler  extends TagSupport{

    @Override
    public int doStartTag() throws JspException {
        try{
            JspWriter out = pageContext.getOut();
            out.println("<h1>this my custom tags<h1>");
            out.println(new Date().toString());
            
        }catch(Exception e){
            e.printStackTrace();
        }
        
        
        
        
        return SKIP_BODY;
    }
   
    
}
