

import java.util.Arrays;

public class square {
    static void square1(int[] arr){
        for (int i = 0; i < arr.length; i++) {
           arr[i]=arr[i]*arr[i];  
        }
        int n=arr.length;
        for (int i = 0; i < n-1; i++) {
            for (int j = 0; j < n-i-1; j++) {
                if(arr[j]>arr[j+1]){
                    swap(arr, j+1, j);
                }
            }
        }
    }
    static void swap(int[] arr,int a,int b){
        int temp=arr[a];
        arr[a]=arr[b];
        arr[b]=temp;
    }
    public static void main(String[] args) {
        int[] arr={1,3,9,2,-7};
        square1(arr);
        System.out.println(Arrays.toString(arr));
    }
}
